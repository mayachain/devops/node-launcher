#!/usr/bin/env bash

set -euo pipefail

MIDGARD_HASHES="https://snapshots.mayachain.info/hashes"

# update mainnet midgard hashes with latest
sed -i.bak '/mayachain-blockstore-hashes/q' midgard/templates/configmap.yaml && rm -f midgard/templates/configmap.yaml.bak
curl -s "$MIDGARD_HASHES" | sed -e 's/^/    /' >>midgard/templates/configmap.yaml
