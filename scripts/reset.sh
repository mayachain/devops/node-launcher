#!/usr/bin/env bash

source ./scripts/core.sh

get_node_info_short
echo "=> Select a MAYANode service to reset"
menu midgard midgard midgard-blockstore binance-daemon mayanode thornode-daemon dash-daemon gaia-daemon kuji-daemon arbitrum-daemon ethereum-daemon-execution ethereum-daemon-beacon avalanche-daemon cardano-daemon radix-daemon
SERVICE=$MENU_SELECTED

if node_exists; then
  echo
  warn "Found an existing MAYANode, make sure this is the node you want to update:"
  display_status
  echo
fi

echo "=> Resetting service $boldyellow$SERVICE$reset of a MAYANode named $boldyellow$NAME$reset"
echo
warn "Destructive command, be careful, your service data volume data will be wiped out and restarted to sync from scratch"
confirm

case $SERVICE in
  midgard)
    kubectl scale -n "$NAME" --replicas=0 sts/midgard-timescaledb --timeout=5m
    kubectl wait --for=delete pods midgard-timescaledb-0 -n "$NAME" --timeout=5m >/dev/null 2>&1 || true
    kubectl run -n "$NAME" -it reset-midgard --rm --restart=Never --image=busybox --overrides='{"apiVersion": "v1", "spec": {"containers": [{"command": ["rm", "-rf", "/var/lib/postgresql/data/pgdata"], "name": "reset-midgard", "stdin": true, "stdinOnce": true, "tty": true, "image": "busybox", "volumeMounts": [{"mountPath": "/var/lib/postgresql/data", "name":"data"}]}], "volumes": [{"name": "data", "persistentVolumeClaim": {"claimName": "data-midgard-timescaledb-0"}}]}}'
    kubectl scale -n "$NAME" --replicas=1 sts/midgard-timescaledb --timeout=5m
    kubectl delete -n "$NAME" pod -l app.kubernetes.io/name=midgard
    ;;

  midgard-blockstore)
    kubectl scale -n "${NAME}" --replicas=0 sts/midgard --timeout=5m
    kubectl wait --for=delete pods midgard-0 -n "${NAME}" --timeout=5m >/dev/null 2>&1 || true
    kubectl run -n "${NAME}" -it reset-midgard --rm --restart=Never --image=busybox --overrides='{"apiVersion": "v1", "spec": {"containers": [{"command": ["sh", "-c", "rm -rf /blockstore/*"], "name": "reset-midgard", "stdin": true, "stdinOnce": true, "tty": true, "image": "busybox", "volumeMounts": [{"mountPath": "/blockstore", "name":"blockstore"}]}], "volumes": [{"name": "blockstore", "persistentVolumeClaim": {"claimName": "blockstore-midgard-0"}}]}}'
    kubectl scale -n "${NAME}" --replicas=1 sts/midgard --timeout=5m
    kubectl delete -n "${NAME}" pod -l app.kubernetes.io/name=midgard
    ;;

  mayanode)
    kubectl scale -n "$NAME" --replicas=0 deploy/mayanode --timeout=5m
    kubectl wait --for=delete pods -l app.kubernetes.io/name=mayanode -n "$NAME" --timeout=5m >/dev/null 2>&1 || true
    kubectl run -n "$NAME" -it recover-mayad --rm --restart=Never --image=busybox --overrides='{"apiVersion": "v1", "spec": {"containers": [{"command": ["sh", "-c", "cd /root/.mayanode/data && rm -rf bak && mkdir -p bak && mv application.db blockstore.db cs.wal evidence.db state.db tx_index.db bak/"], "name": "recover-mayad", "stdin": true, "stdinOnce": true, "tty": true, "image": "busybox", "volumeMounts": [{"mountPath": "/root", "name":"data"}]}], "volumes": [{"name": "data", "persistentVolumeClaim": {"claimName": "mayanode"}}]}}'
    kubectl scale -n "$NAME" --replicas=1 deploy/mayanode --timeout=5m
    ;;

  thornode-daemon)
    kubectl scale -n "$NAME" --replicas=0 deploy/thornode-daemon --timeout=5m
    kubectl wait --for=delete pods -l app.kubernetes.io/name=thornode-daemon -n "$NAME" --timeout=5m >/dev/null 2>&1 || true
    kubectl run -n "$NAME" -it recover-thord --rm --restart=Never --image=busybox --overrides='{"apiVersion": "v1", "spec": {"containers": [{"command": ["sh", "-c", "cd /root/.thornode/data && rm -rf bak && mkdir -p bak && mv application.db blockstore.db cs.wal evidence.db state.db tx_index.db bak/"], "name": "recover-thord", "stdin": true, "stdinOnce": true, "tty": true, "image": "busybox", "volumeMounts": [{"mountPath": "/root", "name":"data"}]}], "volumes": [{"name": "data", "persistentVolumeClaim": {"claimName": "thornode-daemon"}}]}}'
    kubectl scale -n "$NAME" --replicas=1 deploy/thornode-daemon --timeout=5m
    ;;

  binance-daemon)
    kubectl scale -n "$NAME" --replicas=0 deploy/binance-daemon --timeout=5m
    kubectl wait --for=delete pods -l app.kubernetes.io/name=binance-daemon -n "$NAME" --timeout=5m >/dev/null 2>&1 || true
    kubectl run -n "$NAME" -it reset-binance --rm --restart=Never --image=busybox --overrides='{"apiVersion": "v1", "spec": {"containers": [{"command": ["rm", "-rf", "/bnb/config", "/bnb/data", "/bnb/.probe_last_height"], "name": "reset-binance", "stdin": true, "stdinOnce": true, "tty": true, "image": "busybox", "volumeMounts": [{"mountPath": "/bnb", "name":"data"}]}], "volumes": [{"name": "data", "persistentVolumeClaim": {"claimName": "binance-daemon"}}]}}'
    kubectl scale -n "$NAME" --replicas=1 deploy/binance-daemon --timeout=5m
    ;;

  gaia-daemon)
    kubectl scale -n "$NAME" --replicas=0 deploy/gaia-daemon --timeout=5m
    kubectl wait --for=delete pods -l app.kubernetes.io/name=gaia-daemon -n "$NAME" --timeout=5m >/dev/null 2>&1 || true
    kubectl run -n "$NAME" -it reset-gaia --rm --restart=Never --image=busybox --overrides='{"apiVersion": "v1", "spec": {"containers": [{"command": ["sh", "-c", "rm -rf /root/.gaia/data"], "name": "reset-gaia", "stdin": true, "stdinOnce": true, "tty": true, "image": "busybox", "volumeMounts": [{"mountPath": "/root/.gaia", "name":"data"}]}], "volumes": [{"name": "data", "persistentVolumeClaim": {"claimName": "gaia-daemon"}}]}}'
    kubectl scale -n "$NAME" --replicas=1 deploy/gaia-daemon --timeout=5m
    ;;

  kuji-daemon)
    kubectl scale -n "$NAME" --replicas=0 deploy/kuji-daemon --timeout=5m
    kubectl wait --for=delete pods -l app.kubernetes.io/name=kuji-daemon -n "$NAME" --timeout=5m >/dev/null 2>&1 || true
    kubectl run -n "$NAME" -it reset-kuji --rm --restart=Never --image=busybox --overrides='{"apiVersion": "v1", "spec": {"containers": [{"command": ["sh", "-c", "rm -rf /root/.kujira/*"], "name": "reset-kuji", "stdin": true, "stdinOnce": true, "tty": true, "image": "busybox", "volumeMounts": [{"mountPath": "/root/.kujira", "name":"data"}]}], "volumes": [{"name": "data", "persistentVolumeClaim": {"claimName": "kuji-daemon"}}]}}'
    kubectl scale -n "$NAME" --replicas=1 deploy/kuji-daemon --timeout=5m
    ;;

  ethereum-daemon-execution)
    kubectl scale -n "$NAME" --replicas=0 deploy/ethereum-daemon --timeout=5m
    kubectl wait --for=delete pods -l app.kubernetes.io/name=ethereum-daemon -n "$NAME" --timeout=5m >/dev/null 2>&1 || true
    kubectl run -n "$NAME" -it reset-ethereum --rm --restart=Never --image=busybox --overrides='{"apiVersion": "v1", "spec": {"containers": [{"command": ["sh", "-c", "rm -rf /root/.ethereum"], "name": "reset-ethereum", "stdin": true, "stdinOnce": true, "tty": true, "image": "busybox", "volumeMounts": [{"mountPath": "/root", "name":"data"}]}], "volumes": [{"name": "data", "persistentVolumeClaim": {"claimName": "ethereum-daemon"}}]}}'
    kubectl scale -n "$NAME" --replicas=1 deploy/ethereum-daemon --timeout=5m
    ;;

  arbitrum-daemon)
    kubectl scale -n "$NAME" --replicas=0 deploy/arbitrum-daemon --timeout=5m
    kubectl wait --for=delete pods -l app.kubernetes.io/name=arbitrum-daemon -n "$NAME" --timeout=5m >/dev/null 2>&1 || true
    kubectl run -n "$NAME" -it reset-arbitrum --rm --restart=Never --image=busybox --overrides='{"apiVersion": "v1", "spec": {"containers": [{"command": ["sh", "-c", "rm -rf /home/user/.arbitrum/data"], "name": "reset-arbitrum", "stdin": true, "stdinOnce": true, "tty": true, "image": "busybox", "volumeMounts": [{"mountPath": "/home/user/.arbitrum", "name":"data"}]}], "volumes": [{"name": "data", "persistentVolumeClaim": {"claimName": "arbitrum-daemon"}}]}}'
    kubectl scale -n "$NAME" --replicas=1 deploy/arbitrum-daemon --timeout=5m
    ;;

  ethereum-daemon-beacon)
    kubectl scale -n "$NAME" --replicas=0 deploy/ethereum-daemon --timeout=5m
    kubectl wait --for=delete pods -l app.kubernetes.io/name=ethereum-daemon -n "$NAME" --timeout=5m >/dev/null 2>&1 || true
    kubectl run -n "$NAME" -it reset-ethereum --rm --restart=Never --image=busybox --overrides='{"apiVersion": "v1", "spec": {"containers": [{"command": ["sh", "-c", "rm -rf /root/beacon"], "name": "reset-ethereum", "stdin": true, "stdinOnce": true, "tty": true, "image": "busybox", "volumeMounts": [{"mountPath": "/root", "name":"data"}]}], "volumes": [{"name": "data", "persistentVolumeClaim": {"claimName": "ethereum-daemon"}}]}}'
    kubectl scale -n "$NAME" --replicas=1 deploy/ethereum-daemon --timeout=5m
    ;;

  avalanche-daemon)
    kubectl scale -n "$NAME" --replicas=0 deploy/avalanche-daemon --timeout=5m
    kubectl wait --for=delete pods -l app.kubernetes.io/name=avalanche-daemon -n "$NAME" --timeout=5m >/dev/null 2>&1 || true
    kubectl run -n "$NAME" -it reset-avalanche --rm --restart=Never --image=busybox --overrides='{"apiVersion": "v1", "spec": {"containers": [{"command": ["sh", "-c", "rm -rf /root/.avalanchego/db"], "name": "reset-avalanche", "stdin": true, "stdinOnce": true, "tty": true, "image": "busybox", "volumeMounts": [{"mountPath": "/root/.avalanchego", "name":"data"}]}], "volumes": [{"name": "data", "persistentVolumeClaim": {"claimName": "avalanche-daemon"}}]}}'
    kubectl scale -n "$NAME" --replicas=1 deploy/avalanche-daemon --timeout=5m
    ;;

  dash-daemon)
    kubectl scale -n "$NAME" --replicas=0 deploy/dash-daemon --timeout=5m
    kubectl wait --for=delete pods -l app.kubernetes.io/name=dash-daemon -n "$NAME" --timeout=5m >/dev/null 2>&1 || true
    kubectl run -n "$NAME" -it reset-dash --rm --restart=Never --image=busybox --overrides='{"apiVersion": "v1", "spec": {"containers": [{"command": ["sh", "-c", "rm -rf /home/dash/.dashcore/*"], "name": "reset-dash", "stdin": true, "stdinOnce": true, "tty": true, "image": "busybox", "volumeMounts": [{"mountPath": "/home/dash/.dashcore", "name":"data"}]}], "volumes": [{"name": "data", "persistentVolumeClaim": {"claimName": "dash-daemon"}}]}}'
    kubectl scale -n "$NAME" --replicas=1 deploy/dash-daemon --timeout=5m
    ;;

  radix-daemon)
    kubectl scale -n "$NAME" --replicas=0 deploy/radix-daemon --timeout=5m
    kubectl wait --for=delete pods -l app.kubernetes.io/name=radix-daemon -n "$NAME" --timeout=5m >/dev/null 2>&1 || true
    kubectl run -n "$NAME" -it reset-radix --rm --restart=Never --image=busybox --overrides='{"apiVersion": "v1", "spec": {"containers": [{"command": ["sh", "-c", "rm -rf /home/radixdlt/*"], "name": "reset-radix", "stdin": true, "stdinOnce": true, "tty": true, "image": "busybox", "volumeMounts": [{"mountPath": "/home/radixdlt", "name":"data"}]}], "volumes": [{"name": "data", "persistentVolumeClaim": {"claimName": "radix-daemon"}}]}}'
    kubectl scale -n "$NAME" --replicas=1 deploy/radix-daemon --timeout=5m
    ;;

  cardano-daemon)
    kubectl scale -n "$NAME" --replicas=0 deploy/cardano-daemon --timeout=5m
    kubectl wait --for=delete pods -l app.kubernetes.io/name=cardano-daemon -n "$NAME" --timeout=5m >/dev/null 2>&1 || true
    kubectl run -n "$NAME" -it reset-cardano --rm --restart=Never --image=busybox --overrides='{"apiVersion": "v1", "spec": {"containers": [{"command": ["sh", "-c", "rm -rf /root/cardano"], "name": "reset-cardano", "stdin": true, "stdinOnce": true, "tty": true, "image": "busybox", "volumeMounts": [{"mountPath": "/root/cardano", "name":"data"}]}], "volumes": [{"name": "data", "persistentVolumeClaim": {"claimName": "cardano-daemon"}}]}}'
    kubectl scale -n "$NAME" --replicas=1 deploy/cardano-daemon --timeout=5m
    ;;

  bitcoin-daemon)
    kubectl scale -n "${NAME}" --replicas=0 deploy/bitcoin-daemon --timeout=5m
    kubectl wait --for=delete pods -l app.kubernetes.io/name=bitcoin-daemon -n "${NAME}" --timeout=5m >/dev/null 2>&1 || true
    kubectl run -n "${NAME}" -it reset-bitcoin --rm --restart=Never --image=busybox --overrides='{"apiVersion": "v1", "spec": {"containers": [{"command": ["sh", "-c", "rm -rf /home/bitcoin/.bitcoin/*"], "name": "reset-bitcoin", "stdin": true, "stdinOnce": true, "tty": true, "image": "busybox", "volumeMounts": [{"mountPath": "/home/bitcoin/.bitcoin", "name":"data"}]}], "volumes": [{"name": "data", "persistentVolumeClaim": {"claimName": "bitcoin-daemon"}}]}}'
    kubectl scale -n "${NAME}" --replicas=1 deploy/bitcoin-daemon --timeout=5m
    ;;
esac
