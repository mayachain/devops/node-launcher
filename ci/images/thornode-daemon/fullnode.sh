#!/bin/bash

set -o pipefail

export SIGNER_NAME="${SIGNER_NAME:=thorchain}"
export SIGNER_PASSWD="${SIGNER_PASSWD:=password}"

# trunk-ignore(shellcheck/SC1091)
. "$(dirname "$0")/core.sh"

if [ ! -f ~/.thornode/config/genesis.json ]; then
  init_chain
  rm -rf ~/.thornode/config/genesis.json # set in thornode render-config
fi

# render tendermint and cosmos configuration files
thornode render-config

# Edit app.toml to enable grpc at line 162
sed -i '162s/enable = false/enable = true/' ~/.thornode/config/app.toml

exec thornode start
