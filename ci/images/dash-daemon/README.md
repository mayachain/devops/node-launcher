# dash-core

This repackages the official Dash Core docker image with scripts to:
- auto-configure `dash-cli` from Thorchain environment variables
- start a Dash regtest node which auto-generates blocks for Thorchain mocknet.

See the [official documentation](https://hub.docker.com/r/dashpay/dashd) for
usage and configuration options.

## Building

The convention is to build all images with the script at `node-launcher/ci/images/build.sh`.

If you need to manually build dash in isolation you can run:

```bash
docker build -t registry.gitlab.com/thorchain/devops/node-launcher:dash-daemon-$(cat version) .
```

## Running

Mainnet:
```bash
docker run \
  -it \
  --rm \
  --name dash-daemon \
  registry.gitlab.com/thorchain/devops/node-launcher:dash-daemon-$(cat version)
```

Regtest/Mocknet:
```bash
docker run \
  -it \
  --rm \
  --name dash-daemon \
  --entrypoint /scripts/entrypoint-mock.sh \
  registry.gitlab.com/thorchain/devops/node-launcher:dash-daemon-$(cat version)
```

## Command Line RPC

As we have written the dashd config file you can simply run `dash-cli` commands
without any flags:

```bash
docker exec dash-daemon dash-cli getblockchaininfo
```

```bash
docker exec -it dash-daemon bash
dash-cli getnewaddress
```