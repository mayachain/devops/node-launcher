#!/bin/bash
set -eo pipefail

bitcoin-cli -rpcuser=mayachain \
  -rpcpassword=password \
  ping
