apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "cardano-daemon.fullname" . }}
  labels:
    {{- include "cardano-daemon.labels" . | nindent 4 }}
spec:
  replicas: {{ .Values.replicaCount }}
  strategy:
    type: Recreate
  selector:
    matchLabels:
      {{- include "cardano-daemon.selectorLabels" . | nindent 6 }}
  template:
    metadata:
      labels:
        {{- include "cardano-daemon.selectorLabels" . | nindent 8 }}
    spec:
    {{- with .Values.imagePullSecrets }}
      imagePullSecrets:
        {{- toYaml . | nindent 8 }}
    {{- end }}

      serviceAccountName: {{ include "cardano-daemon.serviceAccountName" . }}
      securityContext:
        {{- toYaml .Values.podSecurityContext | nindent 8 }}
      
      initContainers:
        - name: genesis
          image: alpine:{{ .Values.global.images.alpine.tag }}@sha256:{{ .Values.global.images.alpine.hash }}
          command:
            - /scripts/init.sh
          volumeMounts:
            - name: scripts
              mountPath: /scripts
            - name: data
              mountPath: /root/cardano

      containers:
        - name: {{ .Chart.Name }}
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: {{ .Values.image.name }}:{{ include "daemon.tag" . }}@sha256:{{ .Values.image.hash }}
          imagePullPolicy: IfNotPresent
          {{if .Values.nodeCommand}}command:
            {{range .Values.nodeCommand}}- {{. | quote}}
            {{end}}
          {{end}}
          {{if .Values.nodeArgs}}args:
            {{range .Values.nodeArgs}}- {{. | quote}}
            {{end}}
          {{end}}

          env:
          - name: "CARDANO_NODE_SOCKET_PATH"
            value: /ipc/node.socket
          volumeMounts:
            - name: data
              mountPath: /root/cardano
          ports:
            - name: rpc
              containerPort: {{ .Values.service.port }}
              protocol: TCP
          lifecycle:
            preStop:
              exec:
                command:
                  - sh
                  - -c
                  - "killall5 2 && sleep 30" # SIGINT and give 30s for main process to terminate       
          resources:
            {{- toYaml .Values.resources | nindent 12 }}

      volumes:
      - name: data
      {{- if and .Values.persistence.enabled (not .Values.persistence.hostPath) }}
        persistentVolumeClaim:
          claimName: {{ if .Values.persistence.existingClaim }}{{ .Values.persistence.existingClaim }}{{- else }}{{ template "cardano-daemon.fullname" . }}{{- end }}
      {{- else if and .Values.persistence.enabled .Values.persistence.hostPath }}
        hostPath:
          path: {{ .Values.persistence.hostPath }}
          type: DirectoryOrCreate
      {{- else }}
        emptyDir: {}
      {{- end }}
      - name: scripts
        configMap:
          name: {{ include "cardano-daemon.fullname" . }}-scripts
          defaultMode: 0777

    {{- with .Values.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
    {{- end }}
    {{- with .Values.affinity }}
      affinity:
        {{- toYaml . | nindent 8 }}
    {{- end }}
    {{- with .Values.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
    {{- end }}
